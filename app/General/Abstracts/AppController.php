<?php

namespace App\General\Abstracts;

use App\General\Interfaces\IAppController;
use App\Http\Controllers\Controller;
use App\General\Interfaces\IRepository;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

abstract class AppController extends Controller implements IAppController
{
    protected $repository;
    protected $request;

    public function __construct(IRepository $repository)
    {
        $this->repository = $repository;
    }


    public function storeModel(FormRequest $request, string $routeName = null, array $options = [])
    {

        if($request->validated())
        {
            if(!empty($options)){
                switch($options['type']){
                    case "routeModelBinding":
                        $model = $this->repository->store($request->all());
                        $relation = $options['relation'];
                        $id = $model->$relation->id;
                        return redirect()->route($routeName,$id);
                    default:
                        $this->repository->store($request->all());
                        return redirect()->back();    
                }
            }

            $this->repository->store($request->all());
            return redirect()->back();    
        }
    }

    public function deleteModel(Request $request, string $routeName = null, array $options = [])
    {
        if(!empty($options)){
            switch($options['type']){
                case "routeModelBinding":
                    $model = $this->repository->delete($request->all());
                    $relation = $options['relation'];
                    $id = $model->$relation->id;
                    return redirect()->route($routeName,$id);
                default:
                    $this->repository->delete($request->all());
                    return redirect()->back();    
            }
        }

        $this->repository->delete($request->all());
        return redirect()->back();   
    }

    public function update()
    {
        
    }
}