<?php

namespace App\General\Concretes;

use App\General\Interfaces\IConfig;

class ConfigManager {

    protected $config;

    public static $instance;

    private function __construct(IConfig $config)
    {
        $this->config = $config;
    }

    public static function getInstance(IConfig $config)
    {
        if(self::$instance === null)
        {
            self::$instance = new ConfigManager($config);
        }

        return self::$instance;
    }

    public function getConfig()
    {
        return $this->config->getJsonConfig();
    }

}
