<?php

namespace App\General\Concretes\Configurations;

use App\General\Interfaces\IConfig;

class AnotherTestConfiguration implements IConfig
{

    public function getJsonConfig()
    {
        return config('customConfig.AnotherTestConfiguration');
    }
}
