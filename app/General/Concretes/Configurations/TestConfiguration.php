<?php

namespace App\General\Concretes\Configurations;

use App\General\Interfaces\IConfig;

class TestConfiguration implements IConfig
{

    public function getJsonConfig()
    {
        return config('customConfig.TestConfiguration');
    }
}
