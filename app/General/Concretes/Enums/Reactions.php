<?php

namespace App\General\Concretes\Enums;

use App\General\Abstracts\Enum;

class Reactions extends Enum{

    public const LIKE = 'like';
    public const UNLIKE = 'unlike';

    public const LIKE_ID = 0;
    public const UNLIKE_ID = 1;

    public static array $enum = [
        self::LIKE => self::LIKE_ID,
        self::UNLIKE => self::UNLIKE_ID,
    ];

}