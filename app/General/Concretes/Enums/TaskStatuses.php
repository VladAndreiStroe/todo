<?php

namespace App\General\Concretes\Enums;

use App\General\Abstracts\Enum;

class TaskStatuses extends Enum{

    public const PENDING_STATUS = 'pending';
    public const STARTED_STATUS = 'started';
    public const FINISHED_STATUS = 'finished';

    public const PENDING_STATUS_ID = 0;
    public const STARTED_STATUS_ID = 1;
    public const FINISHED_STATUS_ID = 2;

    public static array $enum = [
        self::PENDING_STATUS => self::PENDING_STATUS_ID,
        self::STARTED_STATUS => self::STARTED_STATUS_ID,
        self::FINISHED_STATUS => self::FINISHED_STATUS_ID
    ];

}