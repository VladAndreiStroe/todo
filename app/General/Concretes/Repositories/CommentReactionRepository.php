<?php

namespace App\General\Concretes\Repositories;

use App\General\Abstracts\Repository;
use App\Models\CommentReaction;

class CommentReactionRepository extends Repository
{
    protected $model = CommentReaction::class;
}