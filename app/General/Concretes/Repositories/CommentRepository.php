<?php

namespace App\General\Concretes\Repositories;

use App\General\Abstracts\Repository;
use App\Models\Comment;

class CommentRepository extends Repository
{
    protected $model = Comment::class;
}