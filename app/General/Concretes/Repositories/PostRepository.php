<?php

namespace App\General\Concretes\Repositories;

use App\General\Abstracts\Repository;
use App\Models\Post;

class PostRepository extends Repository
{
    protected $model = Post::class;

    public function getBySlug(string $slug)
    {
        return $this->model::where('slug',$slug)->first();
    }
}
