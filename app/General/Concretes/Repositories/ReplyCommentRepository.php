<?php

namespace App\General\Concretes\Repositories;

use App\General\Abstracts\Repository;
use App\Models\ReplyComment;

class ReplyCommentRepository extends Repository
{
    protected $model = ReplyComment::class;
}