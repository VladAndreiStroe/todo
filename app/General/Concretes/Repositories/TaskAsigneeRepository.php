<?php

namespace App\General\Concretes\Repositories;

use App\General\Abstracts\Repository;
use App\Models\TaskAsignee;

class TaskAsigneeRepository extends Repository{

    protected $model = TaskAsignee::class;
}