<?php

namespace App\General\Concretes\Repositories;

use App\General\Abstracts\Repository;
use App\General\Concretes\Enums\TaskStatuses;
use App\Models\Task;
use App\Models\TaskStatus;

class TaskRepository extends Repository
{
    protected $model = Task::class;

    private $taskStatusRepository;
    private $taskAsigneeRepository;
    
    public function __construct(TaskStatusRepository $taskStatusRepository, TaskAsigneeRepository $taskAsigneeRepository)
    {
        $this->taskStatusRepository = $taskStatusRepository;
        $this->taskAsigneeRepository = $taskAsigneeRepository;
    }

    public function store(array $args)
    {
        $task = parent::store($args);

        if($task !== null && $task instanceof Task){
            $taskStatus = $this->taskStatusRepository->store([
                'task_id' => $task->id,
                'status' => TaskStatuses::PENDING_STATUS_ID,
                'finish_at' => null
            ]);

            foreach($args['asignees'] as $key => $value)
            {
                $taskAsignee = $this->taskAsigneeRepository->store([
                    'task_id' => $task->id,
                    'user_id' => $value
                ]);
            }

            if($taskStatus !== null && $taskStatus instanceof TaskStatus)
            {
                return $task;
            }
            else{
                $task->delete();
                return false;
            }
        }

        return false;
    }

    public function update(array $args)
    {
        $task = parent::update($args);

        if($task !== null && $task instanceof Task){
            if($task->status->status !== $args['status'])
            {
                $taskStatus = $this->taskStatusRepository->update([
                    'modelId' => $task->status->id,
                    'status' => $args['status']
                ]);

                if($taskStatus !== null && $taskStatus instanceof TaskStatus)
                {
                    return $task;
                }
                else{
                    if($task->wasChanged())
                    {
                        $task = parent::update($task->getOriginal());

                        if($task !== null && $task instanceof Task){
                            return $task;
                        }
                        else{
                            return false;
                        }
                    }
                    else{
                        return false;
                    }
                }
            }
            return $task;
        }
        return false;
    }

    public function delete(array $args)
    {
        $task = parent::delete($args);

        if($task !== null && $task instanceof Task){
            $taskStatus = $this->taskStatusRepository->delete([
                'modelId' => $task->status->id
            ]);

            if($taskStatus !== null && $taskStatus instanceof TaskStatus){
                return $task;
            }

            return null;
        }
    }
}