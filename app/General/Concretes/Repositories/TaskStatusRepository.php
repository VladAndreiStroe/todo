<?php

namespace App\General\Concretes\Repositories;

use App\General\Abstracts\Repository;
use App\Models\TaskStatus;

class TaskStatusRepository extends Repository
{
    protected $model = TaskStatus::class;
}