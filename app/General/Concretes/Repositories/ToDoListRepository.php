<?php

namespace App\General\Concretes\Repositories;

use App\General\Abstracts\Repository;
use App\Models\ToDoList;

class ToDoListRepository extends Repository
{
    protected $model = ToDoList::class;
}