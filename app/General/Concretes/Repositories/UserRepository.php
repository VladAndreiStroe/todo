<?php

namespace App\General\Concretes\Repositories;

use App\General\Abstracts\Repository;
use App\Models\User;

class UserRepository extends Repository
{
    protected $model = User::class;
}