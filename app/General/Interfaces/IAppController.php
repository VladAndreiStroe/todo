<?php

namespace App\General\Interfaces;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

interface IAppController
{
    public function storeModel(FormRequest $request, string $routeName = null, array $options = []);
    public function update();
    public function deleteModel(Request $request, string $routeName = null, array $options = []);
}