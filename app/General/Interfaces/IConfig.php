<?php

namespace App\General\Interfaces;

interface IConfig {

    public function getJsonConfig();

}