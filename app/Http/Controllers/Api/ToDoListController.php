<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\General\Concretes\Repositories\ToDoListRepository;
use App\General\Concretes\Repositories\UserRepository;
use App\Models\ToDoList;
use App\Http\Resources\ToDoList as ToDoListResource;
use App\Http\Resources\ToDoListCollection;


class ToDoListController extends Controller
{
    private $toDoListRepository;
    private $userRepository;

    public function __construct(ToDoListRepository $toDoListRepository, UserRepository $userRepository)
    {
        $this->toDoListRepository = $toDoListRepository;
        $this->userRepository = $userRepository;
    }
    
    public function index()
    {
        return response(
            [
                'lists' => new ToDoListCollection($this->toDoListRepository->getAll())
            ]
        );
    }

    public function getList(ToDoList $list)
    {
        return response([
            'list' => new ToDoListResource($list)
        ]);
    }

    public function deleteList(ToDoList $list)
    {
        if($list->delete()){
            return response([
                'success' => true,
                'list' => new ToDoListResource($list)
            ]);
        }        
    }


    public function storeToDoList(Request $request)
    {
        $todoList = $this->toDoListRepository->store($request->all());

        if($todoList instanceof ToDoList){
            return response([
                'success' => true,
                'toDoList' => new ToDoListResource($todoList)
            ],200);
        }  
    }

    public function getUserLists(Request $request)
    {
        $user = $this->userRepository->getById($request->get('user_id'));

        return response([
            'lists' => new ToDoListCollection($user->toDoLists)
        ]);
    }
}
