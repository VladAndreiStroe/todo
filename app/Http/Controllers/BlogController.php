<?php

namespace App\Http\Controllers;

use App\General\Concretes\ConfigManager;
use App\General\Concretes\Configurations\AnotherTestConfiguration;
use App\General\Concretes\Configurations\TestConfiguration;
use App\General\Concretes\Repositories\PostRepository;
use Illuminate\Http\Request;
use App\Models\Post;

class BlogController extends Controller
{

    protected $postRepository;

    public function __construct(PostRepository $postRepository)
    {
        $this->postRepository = $postRepository;
    }

    public function index()
    {
        return view('blog',[
            'posts' => $this->postRepository->getAll()
        ]);
    }

    public function addForm()
    {
        return view('forms.add-new-post-form');
    }

    public function getPost(Request $request)
    {
        $slug = $request->get('slug');

        $post = $this->postRepository->getBySlug($slug);

        return view('post',[
            'post' => $post
        ]);
    }

    public function store(Request $request)
    {
        $post = $this->postRepository->store($request->all());

        if($post instanceof Post)
        {
            return redirect()->back();
        }
    }

    public function test()
    {
        $testConfig = new TestConfiguration();
        $anotherTestConfig = new AnotherTestConfiguration();
        $configManager = ConfigManager::getInstance($anotherTestConfig);



        return $configManager->getConfig();
    }
}
