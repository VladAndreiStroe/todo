<?php

namespace App\Http\Controllers;

use App\General\Abstracts\AppController;
use Illuminate\Http\Request;
use App\General\Concretes\Repositories\CommentRepository;
use App\Http\Requests\AddCommentRequest;
use App\Models\Comment;

class CommentController extends AppController
{
    public function __construct(CommentRepository $commentRepository)
    {
        parent::__construct($commentRepository);
    }

    public function store(AddCommentRequest $request)
    {
        return parent::storeModel($request);
    }
}
