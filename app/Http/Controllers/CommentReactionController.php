<?php

namespace App\Http\Controllers;

use App\General\Abstracts\AppController;
use App\General\Concretes\Repositories\CommentReactionRepository;
use App\Http\Requests\AddCommentReactionRequest;
use App\Models\CommentReaction;
use Illuminate\Support\Facades\Auth;

class CommentReactionController extends AppController
{
    public function __construct(CommentReactionRepository $commentReactionRepository)
    {
        parent::__construct($commentReactionRepository);
    }

    public function store(AddCommentReactionRequest $request)
    {
        $commentId = $request->post('comment_id');
        $replyId = $request->post('reply_id');

        if(!empty($commentId))
        {
           $comment = $this->checkReaction($commentId,'comment');
            
           if($comment !== false){
               $comment->type = $request->post('type');
               $comment->save();
               return redirect()->back();
           }
        }

        if(!empty($replyId))
        {
            $reply = $this->checkReaction($replyId,'reply');

            if($reply !== false){
                $reply->type = $request->post('type');
                $reply->save();
                return redirect()->back();
            }   
        }


        return parent::storeModel($request);
    }

    private function checkReaction(int $id,$reactionType)
    {
        $user = Auth::user();

        foreach($user->commentReactions as $commentReaction)
        {
            switch($reactionType){
                case "comment":
                    if($commentReaction->comment !== null && $commentReaction->comment->id === $id){
                        return $commentReaction;
                    }
                    break;
                case "reply":
                    if($commentReaction->reply !== null && $commentReaction->reply->id === $id){
                        return $commentReaction;
                    }
                    break;
            }
        }

        return false;
    }
}
