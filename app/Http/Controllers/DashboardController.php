<?php

namespace App\Http\Controllers;

use App\General\Concretes\Enums\TaskStatuses;
use App\General\Concretes\Repositories\TaskRepository;
use App\General\Concretes\Repositories\TaskStatusRepository;
use App\General\Concretes\Repositories\ToDoListRepository;
use App\General\Interfaces\IRepository;
use App\Models\TaskStatus;
use Illuminate\Http\Request;

class DashboardController extends Controller
{

    private $todoListRepository;
    private $taskRepository;
    private $taskStatusRepository;

    public function __construct(ToDoListRepository $todoListRepository, TaskRepository $taskRepository, TaskStatusRepository $taskStatusRepository)
    {
        $this->todoListRepository = $todoListRepository;
        $this->taskRepository = $taskRepository;
        $this->taskStatusRepository = $taskStatusRepository;
    }

    public function index()
    {

        $todoLists = $this->todoListRepository->getAll();
        $tasks = $this->taskRepository->getAll();
        $taskStatuses = $this->taskStatusRepository->getAll();
        $statusCategories = [];

        $statusCategories = $taskStatuses->groupBy('status');

        $categories = [];

        foreach($statusCategories as $key=>$value){
            $categories[TaskStatuses::getValueById($key)] = count($value);
        }

        return view('dashboard',[
            'todoLists' => $todoLists,
            'tasks' => [
                'nrOfTasks' => count($tasks),
                'categories' => $categories
            ]
        ]);
    }
}
