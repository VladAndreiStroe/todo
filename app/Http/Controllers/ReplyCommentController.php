<?php

namespace App\Http\Controllers;

use App\General\Abstracts\AppController;
use Illuminate\Http\Request;
use App\General\Concretes\Repositories\ReplyCommentRepository;
use App\Http\Requests\ReplyCommentRequest;
use App\Models\ReplyComment;

class ReplyCommentController extends AppController
{
    public function __construct(ReplyCommentRepository $replyCommentRepository)
    {
        parent::__construct($replyCommentRepository,);
    }

    public function store(ReplyCommentRequest $request)
    {
        return parent::storeModel($request);
    }
}
