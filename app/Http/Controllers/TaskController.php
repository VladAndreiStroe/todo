<?php

namespace App\Http\Controllers;

use App\General\Abstracts\AppController;
use App\General\Concretes\Repositories\TaskRepository;
use App\Http\Requests\AddNewTaskRequest;
use App\Http\Requests\UpdateTaskRequest;
use App\Models\Task;
use App\Models\TaskPicture;
use App\Models\ToDoList;
use Illuminate\Http\Request;
use App\General\Concretes\Repositories\UserRepository;

class TaskController extends AppController
{
    private $userRepository;

    public function __construct(TaskRepository $taskRepository, UserRepository $userRepository)
    {
        parent::__construct($taskRepository);

        $this->userRepository = $userRepository;
    }

    public function addNewTaskView(ToDoList $list)
    {
        $users = $this->userRepository->getAll();

        return view('forms.add-new-task-form',[
            'list' => $list,
            'users' => $users
        ]);
    }

    public function viewTaskView(Task $task)
    {
        return view('task-view',[
            'task' => $task
        ]);
    }

    public function store(AddNewTaskRequest $request)
    {
//        return parent::storeModel($request,"view-list-details",[
//            'type' => "routeModelBinding",
//            'relation' => 'toDoList'
//        ]);
        if($request->validated()){

            $files = $request->file('pictures');

            $task = $this->repository->store($request->all());

            if($task instanceof Task){
                foreach ($files as $file)
                {
                    $picture = new TaskPicture([
                        'task_id' => $task->id,
                        'picture_path' => ""
                    ]);

                    $picture->save();

                    $path = $file->storeAs(
                        'public/images/tasks', "task_". $task->id. "_" . $picture->id .".".$file->extension()
                    );

                    $imagePath = explode('public',$path);

                    $picture->picture_path = "/storage".$imagePath[1];

                    $picture->save();
                }

                return redirect()->back();
            }


        }
    }


    public function updateTaskView(Task $task)
    {
        $users = $this->userRepository->getAll();

        return view('forms.edit-task-form',[
            'task' => $task,
            'users' => $users
        ]);
    }

    public function updateTask(UpdateTaskRequest $request)
    {
        if($request->validated()){

            $task = $this->repository->getById($request->post('modelId'));

            $originalValues = $task->getOriginal();
            $args = $request->all();

            if(
                $originalValues['title'] !== $args['title'] ||
                $originalValues['description'] !== $args['description'] ||
                $task->status->status !== (int) $args['status'])
                {
                $task = $this->repository->update($args);

                if($task !== false && $task instanceof Task){
                    return redirect()->route('view-list-details',$task->toDoList->id);
                }
            }

            return redirect()->route('view-list-details',$task->toDoList->id);
        }
    }

    public function delete(Request $request)
    {
        return parent::deleteModel($request,'view-list-details',[
            'type' => "routeModelBinding",
            'relation' => 'toDoList'
        ]);
    }
}
