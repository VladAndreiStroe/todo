<?php

namespace App\Http\Controllers;

use App\General\Concretes\Repositories\TaskStatusRepository;
use Illuminate\Http\Request;

class TaskStatusController extends Controller
{
    private $taskStatusRepository;

    public function __construct(TaskStatusRepository $taskStatusRepository)
    {
        $this->taskStatusRepository = $taskStatusRepository;
    }
}
