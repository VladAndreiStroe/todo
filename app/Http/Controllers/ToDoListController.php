<?php

namespace App\Http\Controllers;

use App\General\Concretes\Repositories\ToDoListRepository;
use App\Http\Requests\AddNewListRequest;
use App\Models\ToDoList;
use Illuminate\Http\Request;

class ToDoListController extends Controller
{
    private $toDoListRepository;

    public function __construct(ToDoListRepository $toDoListRepository)
    {
        $this->toDoListRepository = $toDoListRepository;
    }

    public function index()
    {
        return view('to-do-lists',[
            'todoLists' => $this->toDoListRepository->getAll()
        ]);
    }

    public function addNewListForm()
    {
        return view('forms.add-new-list-form');
    }

    public function editList(Request $request)
    {
        return view('forms.edit-list-form',[
            'list' => $this->toDoListRepository->getById($request->get('list'))
        ]);
    }


    public function store(AddNewListRequest $request)
    {
        if($request->validated()){

            $todoList = $this->toDoListRepository->store($request->all());

            if($todoList !== null){
                if($todoList instanceof ToDoList){
                    return view("forms.add-new-list-form",[
                        'success' => true
                    ]);
                }else{
                    
                }
            }
        }
    }

    public function update(AddNewListRequest $request)
    {
        if($request->validated())
        {
            $todoList = $this->toDoListRepository->update($request->all());

            if($todoList !== null && $todoList instanceof ToDoList){
                return redirect()->route('to-do-lists-edit-form', ['list' => $todoList->id])->with('success',true);
            }
        }
    }

    public function delete(Request $request)
    {
        if($this->toDoListRepository->delete($request->all()))
            return redirect()->route('to-do-lists');
    }

    public function viewListDetails(ToDoList $list)
    {
        return view('to-do-list',[
            'list' => $list
        ]);
    }
}
