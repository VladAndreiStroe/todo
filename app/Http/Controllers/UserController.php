<?php

namespace App\Http\Controllers;

use App\General\Concretes\Repositories\UserRepository;
use App\Http\Requests\AddUserProfilePictureRequest;
use App\Http\Requests\UserUpdateRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{

    private $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function userProfileView(User $user)
    {
        if(!$user->id){
            return view('user-profile',[
                'user' => Auth::user(),
                'isAuth' => true
            ]);
        }else{
            return view('user-profile',[
                'user' => $this->userRepository->getById($user->id),
                'isAuth' => false
            ]);
        }
    }

    public function updateUser(UserUpdateRequest $request)
    {
        if($request->validated())
        {
            $user = $this->userRepository->update($request->all());

            if($user !== null && $user instanceof User){
                return redirect()->route('user-profile');
            }
        }   
    }

    public function addProfilePicture(AddUserProfilePictureRequest $request)
    {
        if($request->validated())
        {
            $user = Auth::user();

            $path = $request->file('profile_picture')->storeAs(
                'public/images/profile', "profile_". $user->id.".".$request->file('profile_picture')->extension()
            );

            $imagePath = explode('public',$path);

            $user->profile_picture = "/storage".$imagePath[1];

            $user->save();

            return redirect()->back();
        }
    }
}
