<?php

namespace App\Models;

use App\General\Concretes\Enums\Reactions;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Comment extends Model
{
    use HasFactory;

    protected $fillable = [
        'comment',
        'user_id',
        'task_id'
    ];

    protected $dates = [
        'created_at',
        'updated_at'
    ];

    public function task()
    {
        return $this->belongsTo(Task::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function replies()
    {
        return $this->hasMany(ReplyComment::class);
    }

    public function commentReactions()
    {
        return $this->hasMany(CommentReaction::class);
    }

    public function getLikesAttribute()
    {
        return $this->commentReactions()->where('type',Reactions::LIKE_ID)->get();
    }

    public function getUnlikesAttribute()
    {
        return $this->commentReactions()->where('type',Reactions::UNLIKE_ID)->get();
    }

    public function getUserHasLikedAttribute()
    {
        $user = Auth::user();

        foreach($this->likes as $like)
        {
            if($like->user->id === $user->id)
            {
                return true;
            }
        }

        return false;
    }

    public function getUserHasUnlikedAttribute()
    {
        $user = Auth::user();

        foreach($this->unlikes as $unlike)
        {
            if($unlike->user->id === $user->id)
            {
                return true;
            }
        }

        return false;
    }
}
