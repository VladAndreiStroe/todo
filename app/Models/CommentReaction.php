<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CommentReaction extends Model
{
    use HasFactory;

    protected $fillable = [
        'type',
        'comment_id',
        'reply_id',
        'user_id'
    ];

    protected $dates = [
        'created_at',
        'updated_at'
    ];

    public function comment()
    {
        return $this->belongsTo(Comment::class);
    }

    public function reply()
    {
        return $this->belongsTo(ReplyComment::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
