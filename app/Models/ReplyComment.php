<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\General\Concretes\Enums\Reactions;
use Illuminate\Support\Facades\Auth;

class ReplyComment extends Model
{
    use HasFactory;

    protected $fillable = [
        'comment_id',
        'user_id',
        'reply'
    ];

    protected $dates = [
        'created_at',
        'updated_at'
    ];

    public function comment()
    {
        return $this->belongsTo(Comment::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function commentReactions()
    {
        return $this->hasMany(CommentReaction::class,'reply_id','id');
    }

    public function getLikesAttribute()
    {
        return $this->commentReactions()->where('type',Reactions::LIKE_ID)->get();
    }

    public function getUnlikesAttribute()
    {
        return $this->commentReactions()->where('type',Reactions::UNLIKE_ID)->get();
    }

    public function getUserHasLikedAttribute()
    {
        $user = Auth::user();

        foreach($this->likes as $like)
        {
            if($like->user->id === $user->id)
            {
                return true;
            }
        }

        return false;
    }

    public function getUserHasUnlikedAttribute()
    {
        $user = Auth::user();

        foreach($this->unlikes as $unlike)
        {
            if($unlike->user->id === $user->id)
            {
                return true;
            }
        }

        return false;
    }
}
