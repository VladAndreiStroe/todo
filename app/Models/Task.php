<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'description',
        'user_id',
        'to_do_list_id',
        'deadline'
    ];

    protected $dates = [
        'deadline',
        'created_at',
        'updated_at'
    ];

    public function toDoList()
    {
        return $this->belongsTo(ToDoList::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function status()
    {
        return $this->hasOne(TaskStatus::class);
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    public function taskAsignees()
    {
        return $this->hasMany(TaskAsignee::class);
    }

    public function pictures()
    {
        return $this->hasMany(TaskPicture::class);
    }

    public function getTaskAsigneesIdsAttribute()
    {
        $array = [];

        $taskAsignees = $this->taskAsignees;

        foreach($taskAsignees as $taskAsignee)
        {
            $array[] = $taskAsignee->user->id;
        }

        return $array;
    }
}
