<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TaskPicture extends Model
{
    use HasFactory;

    protected $fillable = [
        'task_id',
        'picture_path'
    ];

    protected $dates = [
        'created_at',
        'updated_at'
    ];

    public function task()
    {
        return $this->belongsTo(Task::class);
    }
}
