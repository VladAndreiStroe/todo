<?php

namespace App\Models;

use App\General\Concretes\Enums\TaskStatuses;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TaskStatus extends Model
{
    use HasFactory;

    protected $fillable = [
        'task_id',
        'status',
        'finish_at'
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'finish_at'
    ];

    public function task()
    {
        return $this->belongsTo(Task::class);
    }

    public function getStatusNameAttribute()
    {
        return ucfirst(TaskStatuses::getValueById($this->status));
    }
}
