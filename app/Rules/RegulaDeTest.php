<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class RegulaDeTest implements Rule
{
    protected $message;
    protected $args;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($args = [])
    {
        $this->args = $args;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if(strlen($value) > 4){
            $this->message = 'Mesaj din regula. :attribute Trebuie sa fie mai mare ca 4';
            return true;
        }

        return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->message;
    }
}
