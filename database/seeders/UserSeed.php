<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User([
            'name' => "Vlad",
            "email" => "email@gmail.com",
            "password" => Hash::make("12345678")
        ]);

        $user->save();
    }
}
