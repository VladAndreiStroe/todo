<x-app-layout>
    <x-slot name="header">
        <h2 class="h4 font-weight-bold">
           Blog
        </h2>
    </x-slot>

    <div class="card my-4">
        <div class="card-body p-4">
            <div class="row mb-4">
                <div class="col">
                    <a href="{{route('add-post-form')}}" class="btn btn-primary float-right">Adauga Post</a>
                </div>
            </div>
            <div class="row">
                @foreach($posts as $post)
                    <div class="col-3">
                        <div class="card" style="box-shadow:1px 1px 15px #ccc;">
                            <div class="card-body">
                                <p>{{$post->title}}</p>
                            </div>
                            <div class="card-footer">
                                <a href="{{route('get-post',['slug' => $post->slug])}}" class="btn btn-primary">Vezi post</a>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>

</x-app-layout>