@props(['user'])

<div class="card my-4">
    <div class="card-body">
        <ul class="nav nav-tabs" id="myTab" role="tablist">
            <li class="nav-item" role="presentation">
              <a class="nav-link active" id="comments-tab" data-toggle="tab" href="#comments" role="tab" aria-controls="comments" aria-selected="true">Comments</a>
            </li>
            <li class="nav-item" role="presentation">
              <a class="nav-link" id="replies-tab" data-toggle="tab" href="#replies" role="tab" aria-controls="replies" aria-selected="false">Replies</a>
            </li>
            <li class="nav-item" role="presentation">
              <a class="nav-link" id="reactions-tab" data-toggle="tab" href="#reactions" role="tab" aria-controls="reactions" aria-selected="false">Reactions</a>
            </li>
            <li class="nav-item" role="presentation">
                <a class="nav-link" id="tasks-tab" data-toggle="tab" href="#tasks" role="tab" aria-controls="tasks" aria-selected="false">Tasks</a>
              </li>
          </ul>

        <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade show active" id="comments" role="tabpanel" aria-labelledby="comments-tab">
                @foreach($user->comments as $comment)
                <div class="card my-4" style="background-color:#f8fafc; border:1px solid #e4e4e4">
                    <div class="card-body">
                        <p>{{$comment->comment}}</p>
                    </div>
                    <div class="card-footer">
                        <p class="float-right mb-0">Adaugat la: {{$comment->created_at}}</p>
                    </div>
                </div>
                @endforeach
            </div>
            <div class="tab-pane fade" id="replies" role="tabpanel" aria-labelledby="replies-tab">
                <div class="tab-pane fade show active" id="comments" role="tabpanel" aria-labelledby="comments-tab">
                    @foreach($user->replies as $reply)
                    <div class="card my-4" style="background-color:#f8fafc; border:1px solid #e4e4e4">
                        <div class="card-header">
                            <p>{{$reply->comment->comment}}</p>
                            <span class="float-right">Adaugat de: {{$reply->comment->user->name}} la: {{$reply->comment->created_at}}</span>
                        </div>
                        <div class="card-body">
                            <p>{{$reply->reply}}</p>
                        </div>
                        <div class="card-footer">
                            <p class="float-right mb-0">Adaugat la: {{$reply->created_at}}</p>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
            <div class="tab-pane fade" id="reactions" role="tabpanel" aria-labelledby="reactions-tab">
                <div class="row">
                    <div class="col-6">
                        <div class="card my-4" style="background-color:#f8fafc; border:1px solid #e4e4e4">
                            <i class="bi bi-hand-thumbs-up"></i> <span class="badge badge-light">{{count($user->userLikes)}}</span>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="card my-4" style="background-color:#f8fafc; border:1px solid #e4e4e4">
                            <i class="bi bi-hand-thumbs-down"></i><span class="badge badge-light">{{count($user->userUnlikes)}}</span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        @foreach ($user->commentReactions as $commentReaction)
                            <div class="card my-4" style="background-color:#f8fafc; border:1px solid #e4e4e4">
                                @if(!empty($commentReaction->comment))
                                    <div class="card-header text-center">
                                        <p><i class="bi {{$commentReaction->type === 0 ? "bi-hand-thumbs-up" : "bi-hand-thumbs-down"}}"></i></p>
                                    </div>
                                    <div class="card-body">
                                        <p>{{$commentReaction->comment->comment}}</p>
                                    </div>
                                @endif

                                @if(!empty($commentReaction->reply))
                                <div class="card-header text-center">
                                    <p><i class="bi {{$commentReaction->type === 0 ? "bi-hand-thumbs-up" : "bi-hand-thumbs-down"}}"></i></p>
                                </div>
                                    <div class="card-body">
                                        <p>{{$commentReaction->reply->reply}}</p>
                                    </div>
                                @endif
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="tab-pane fade show active" id="tasks" role="tabpanel" aria-labelledby="tasks-tab">
                @foreach($user->taskAsignees as $taskAsignee)
                <div class="card my-4" style="background-color:#f8fafc; border:1px solid #e4e4e4">
                    <div class="card-header">
                        <a href="{{route('view-task',$taskAsignee->task->id)}}" class="btn btn-primary btn-sm float-right">Go to task</a>
                        <a href="{{route('update-task-view',$taskAsignee->task->id)}}" class="btn btn-primary btn-sm float-right mr-2">Edit task</a>
                        <p>{{$taskAsignee->task->title}}</p>
                    </div>
                    <div class="card-body">
                        {{$taskAsignee->task->description}}
                    </div>
                </div>
                @endforeach
            </div>
          </div>
    </div>
</div>