@props(['user','isAuth'])

<div class="card my-4">
    <div class="card-body">
        @if(isset($success))
        @if($success === true)
            <p>Lista salvata cu succes</p>
        @endif
        @if($success === false)
            <p>A aparut o eroare</p>
        @endif
        @endif
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul class="mb-0">
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="row">
            <div class="col-12">
                @if ($user->profile_picture)
                    <img src={{$user->profile_picture}} class="img-fluid" />
                @else
                    <img src="/images/profile/default.png" class="img-fluid" />
                @endif
            </div>
            <div class="col-12">
                @if($isAuth)
                    <form action="{{route('add-profile-picture')}}" method="post" enctype="multipart/form-data" class="mt-2">
                        @csrf
                        <input type="file" class="form-control" name="profile_picture" />
                        <button type="submit" class="btn btn-primary float-right mt-2">Add</button>
                    </form>
                @endif
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                @if($isAuth)
                    <form method="POST" action="{{route('user-profile-update')}}">
                        @csrf
                        <input type="hidden" name="modelId" value="{{$user->id}}" />
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input class="form-control" aria-describedby="nameHelp" type="text" name="name" value="{{$user->name}}"/>
                            <small id="nameHelp" class="form-text text-muted">Name of the user</small>
                        </div>
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input class="form-control" aria-describedby="emailHelp" type="email" name="email" value="{{$user->email}}"/>
                            <small id="emailHelp" class="form-text text-muted">Email of the user</small>
                        </div>
                        <button type="submit" class="btn btn-primary">Edit</button>
                    </form>                       
                @else
                    <p>Name: {{$user->name}}</p>
                    <p>Email: {{$user->email}}</p>
                @endif

            </div>
        </div>
        
    </div>
</div>