<x-app-layout>
    <x-slot name="header">
        <h2 class="h4 font-weight-bold">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>

    <div class="row">
        <div class="col-3">
            <div class="card my-4">
                <div class="card-body">
                    <h3>Nr of lists: {{count($todoLists)}}</h3>
                </div>
            </div>
        </div>
        <div class="col-3">
            <div class="card my-4">
                <div class="card-body">
                    <h3>Nr of tasks: {{$tasks['nrOfTasks']}}</h3>
                </div>
            </div>
        </div>
        <div class="col-3">
            <div class="card my-4">
                <div class="card-body">
                    <h3>Tasks:</h3>
                    <ul>
                        @foreach($tasks['categories'] as $key => $value)
                        <li>{{ucfirst($key)}}:{{$value}}</li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-3">
            <div class="card my-4">
                <div class="card-body">
                    <h3>Tasks:</h3>
                    <ul>
                        @foreach($tasks['categories'] as $key => $value)
                        <li>{{ucfirst($key)}}:{{$value}}</li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>