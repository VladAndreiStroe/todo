<x-app-layout>
    <x-slot name="header">
        <h2 class="h4 font-weight-bold">
            Add new task for list: <strong>{{$list->title}}</strong>
        </h2>
    </x-slot>

    <div class="card my-4">
        <div class="card-body">
            <div class="row justify-content-center">
                <div class="col col-4">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul class="mb-0">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <form method="POST" action="{{route('store-new-task')}}" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="to_do_list_id" value="{{$list->id}}">
                        <input type="hidden" name="user_id" value="{{Auth::user()->id}}">
                        <div class="form-group">
                            <label for="title">Title</label>
                            <input class="form-control" aria-describedby="titleHelp" type="text" name="title" />
                            <small id="titleHelp" class="form-text text-muted">The title of task</small>
                        </div>
                        <div class="form-group">
                            <label for="description">Description</label>
                            <textarea class="form-control" aria-describedby="descriptionHelp" name="description"></textarea>
                            <small id="descriptionHelp" class="form-text text-muted">The description of task</small>
                        </div>
                        <div class="form-group">
                            <label for="asignees">Asignees</label>
                            <select class="form-control" multiple name="asignees[]">
                                @foreach ($users as $user)
                                    <option value="{{$user->id}}">{{$user->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="deadline">Deadline</label>
                            <input type='date' class="form-control" name='deadline' />
                        </div>
                        <div class="form-group">
                            <label>Pictures</label>
                            <input type="file" name="pictures[]" class="form-control" multiple />
                        </div>
                        <button type="submit" class="btn btn-primary">Add</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

</x-app-layout>
