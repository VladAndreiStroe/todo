<x-app-layout>
    <x-slot name="header">
        <h2 class="h4 font-weight-bold">
            Edit list: {{$list->title}}
        </h2>
    </x-slot>

    <div class="card my-4">
        <div class="card-body">
            <div class="row justify-content-center">
                <div class="col col-4">
                    @if(isset($success))
                    @if($success === true)
                        <p>Lista salvata cu succes</p>
                    @endif
                    @if($success === false)
                        <p>A aparut o eroare</p>
                    @endif
                    @endif
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul class="mb-0">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <form method="POST" action="{{route('to-do-lists-edit')}}">
                        @csrf
                        <input type="hidden" name="modelId" value="{{$list->id}}" />
                        <div class="form-group">
                            <label for="title">Title</label>
                            <input class="form-control" aria-describedby="titleHelp" type="text" name="title" value="{{$list->title}}"/>
                            <small id="titleHelp" class="form-text text-muted">The title of the to do list</small>
                        </div>
                        <button type="submit" class="btn btn-primary">Edit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>