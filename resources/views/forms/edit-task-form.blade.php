<x-app-layout>
    <x-slot name="header">
        <h2 class="h4 font-weight-bold">
           Edit task: <strong>{{$task->title}}</strong>
        </h2>
    </x-slot>

    <div class="card my-4">
        <div class="card-body">
            <div class="row justify-content-center">
                <div class="col col-4">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul class="mb-0">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <form method="POST" action="{{route('update-task')}}">
                        @csrf
                        <input type="hidden" name="modelId" value="{{$task->id}}">
                        <div class="form-group">
                            <label for="title">Title</label>
                            <input class="form-control" aria-describedby="titleHelp" type="text" name="title" value="{{$task->title}}"/>
                            <small id="titleHelp" class="form-text text-muted">The title of task</small>
                        </div>
                        <div class="form-group">
                            <label for="description">Description</label>
                            <textarea class="form-control" aria-describedby="descriptionHelp" name="description">{{$task->description}}</textarea>
                            <small id="descriptionHelp" class="form-text text-muted">The description of task</small>
                        </div>
                        <div class="form-group">
                            <label for="asignees">Asignees</label>
                            <select class="form-control" multiple name="asignees[]">
                                @foreach ($users as $user)
                                    <option value="{{$user->id}}" @if(in_array($user->id,$task->taskAsigneesIds)) selected @endif>{{$user->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="statuses">Status</label>
                            <select id="statuses" name="status" class="custom-select">
                                @foreach(App\General\Concretes\Enums\TaskStatuses::$enum as $key => $value)
                                    <option value="{{$value}}" @if($task->status->status == $value) selected @endif>{{ucfirst($key)}}</option>
                                @endforeach
                            </select>
                        </div>
                        <button type="submit" class="btn btn-primary">Update</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

</x-app-layout>