<x-app-layout>
    <x-slot name="header">
        <h2 class="h4 font-weight-bold">
            {{$post->title}}
        </h2>
    </x-slot>

    <div class="card my-4">
        <div class="card-body">
            <div id="editor"></div>
        </div>
    </div>

@section('javascripts')
    <script>
        const editorJs = new EditorJS({
            holderId : 'editor',
            tools: { 
                header: Header, 
                list: List 
            }, 
            onReady: () => {
                console.log('Editor.js is ready to work!')
            },
            data: {!! $post->content !!},
            readOnly: true
        })
    </script>
@endsection

</x-app-layout>
