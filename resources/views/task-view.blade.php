<x-app-layout>
    <x-slot name="header">
        <h2 class="h4 font-weight-bold">
           Task: {{$task->title}} <span class="badge badge-primary float-right">{{ucfirst(App\General\Concretes\Enums\TaskStatuses::getValueById($task->status->status))}}</span>
        </h2>
    </x-slot>

    <div class="card my-4">
        <div class="card-body">
              <p>Description: {{$task->description}}</p>
              <p>Asignees: @foreach ($task->taskAsignees as $asignee)
                    @if($asignee->user->id === Auth::user()->id)
                    <a href="{{route('user-profile')}}"><span class="badge badge-primary p-2">{{$asignee->user->name}}</span></a>
                    @else
                    <a href="{{route('user-profile-noauth',$asignee->user->id)}}"><span class="badge badge-secondary p-2">{{$asignee->user->name}}</span></a>
                    @endif

              @endforeach
            <div class="row">
                @foreach($task->pictures as $picture)
                    <div class="col-3">
                        <img src="{{$picture->picture_path}}" class="img-fluid" />
                    </div>
                @endforeach
            </div>
        </div>
    </div>

    <div class="card my-4">
        <div class="card-body">
            <form method="POST" action="{{route('add-comment')}}">
                @csrf
                <input type="hidden" name="task_id" value="{{$task->id}}" />
                <input type="hidden" name="user_id" value="{{Auth::user()->id}}" />
                <div class="form-group">
                    <label for="comment">Comment</label>
                    <textarea class="form-control" type="text" name="comment" rows="3"></textarea>
                </div>
                <button type="submit" class="btn btn-primary">Add comment</button>
            </form>
        </div>
    </div>

    @foreach($task->comments as $comment)
        <div class="card my-4">
            <div class="card-body">

                <form method="POST" action="{{route('add-reaction')}}" class="float-right">
                    @csrf
                    <input type="hidden" name="type" value="{{App\General\Concretes\Enums\Reactions::UNLIKE_ID}}" />
                    <input type="hidden" name="user_id" value="{{Auth::user()->id}}" />
                    <input type="hidden" name="comment_id" value="{{$comment->id}}" />
                    <button type="submit" class="btn btn-sm btn-danger" {{$comment->userHasUnliked ? 'disabled' : ''}}><i class="bi bi-hand-thumbs-down"></i> <span class="badge badge-light">{{count($comment->unlikes)}}</span></button>
                </form>

                <form method="POST" action="{{route('add-reaction')}}" class="float-right">
                    @csrf
                    <input type="hidden" name="type" value="{{App\General\Concretes\Enums\Reactions::LIKE_ID}}" />
                    <input type="hidden" name="user_id" value="{{Auth::user()->id}}" />
                    <input type="hidden" name="comment_id" value="{{$comment->id}}" />
                    <button type="submit" class="btn btn-sm btn-success" {{$comment->userHasLiked ? 'disabled' : ''}}><i class="bi bi-hand-thumbs-up"></i> <span class="badge badge-light">{{count($comment->likes)}}</span></button>
                </form>

                <p>{{$comment->comment}}</p>
            </div>
            <div class="card-footer">
                <p class="mb-0">Created by: {{$comment->user->name}}</p>
                <span>At: {{$comment->created_at->toRfc3339String()}}</span>
                <input type="datetime-local" value="{{$comment->created_at->toRfc3339String()}}" />
                <button class="btn btn-primary float-right" type="button" data-toggle="collapse" data-target="#replyForm-{{$comment->id}}" aria-expanded="false" aria-controls="replyForm">
                    Add reply
                </button>

                <div class="collapse my-4" id="replyForm-{{$comment->id}}">
                    <div class="card card-body">
                        <form method="POST" action="{{route('add-reply')}}">
                            @csrf
                            <input type="hidden" name="comment_id" value="{{$comment->id}}" />
                            <input type="hidden" name="user_id" value="{{Auth::user()->id}}" />
                            <div class="form-group">
                                <label for="comment">Reply</label>
                                <textarea class="form-control" type="text" name="reply" rows="3"></textarea>
                            </div>
                            <button type="submit" class="btn btn-primary">Add reply</button>
                        </form>
                    </div>
                </div>

                @foreach ($comment->replies as $reply)
                    <div class="card card-body my-4">
                        <div>
                            <form method="POST" action="{{route('add-reaction')}}" class="float-right">
                                @csrf
                                <input type="hidden" name="type" value="{{App\General\Concretes\Enums\Reactions::UNLIKE_ID}}" />
                                <input type="hidden" name="user_id" value="{{Auth::user()->id}}" />
                                <input type="hidden" name="reply_id" value="{{$reply->id}}" />
                                <button type="submit" class="btn btn-sm btn-danger" {{$reply->userHasUnliked ? 'disabled' : ''}}><i class="bi bi-hand-thumbs-down"></i> <span class="badge badge-light">{{count($reply->unlikes)}}</span></button>
                            </form>

                            <form method="POST" action="{{route('add-reaction')}}" class="float-right">
                                @csrf
                                <input type="hidden" name="type" value="{{App\General\Concretes\Enums\Reactions::LIKE_ID}}" />
                                <input type="hidden" name="user_id" value="{{Auth::user()->id}}" />
                                <input type="hidden" name="reply_id" value="{{$reply->id}}" />
                                <button type="submit" class="btn btn-sm btn-success" {{$reply->userHasLiked ? 'disabled' : ''}}><i class="bi bi-hand-thumbs-up"></i> <span class="badge badge-light">{{count($reply->likes)}}</span></button>
                            </form>

                            <p>{{$reply->reply}}</p>
                        </div>
                        <div>
                            <span class="badge badge-success float-right p-2">{{$reply->user->name}} : {{$reply->created_at}}</span>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    @endforeach
</x-app-layout>
