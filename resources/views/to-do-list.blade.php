<x-app-layout>
    <x-slot name="header">
        <h2 class="h4 font-weight-bold">
           To do lists
        </h2>
    </x-slot>

    <div class="card my-4">
        <div class="card-body">
            <div class="row">
                <div class="col">
                    <h3>List title: <b>{{$list->title}}</b></h3>                    
                </div>
            </div>
            <hr>
            <div class="row justify-content-end mb-4">
                <div class="col-4">
                    <a href="{{route('add-new-task-view',$list->id)}}" class="btn btn-primary float-right">Adauga task</a>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    @if(count($list->tasks) === 0)
                        <h2 class="text-center">No tasks available</h2>
                    @else
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Title</th>
                                <th>Description</th>
                                <th>User</th>
                                <th>Status</th>
                                <th>Deadline</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($list->tasks as $task)
                           
                                <tr style="margin:auto">
                                    <td style="margin:auto">{{$task->title}}</td>
                                    <td>{{$task->description}}</td>
                                    <td>{{$task->user->email}}</td>
                                    <td>
                                        @if (!empty($task->status))
                                            
                                        {{$task->status->statusName}}
                                            
                                        @endif
                                    </td>
                                    <td>{{$task->deadline->diffForHumans()}}</td>
                                    <td>
                                        <a href="{{route('view-task',$task->id)}}" class="btn btn-info btn-sm">View</a>
                                        <a href="{{route('update-task-view',$task->id)}}" class="btn btn-primary btn-sm">Edit</a>
                                        <form method="POST" action="{{route('delete-task')}}" style="display:inline-block">
                                            @csrf
                                            <input type="hidden" value="{{$task->id}}" name="modelId" />
                                            <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    @endif
                    
                </div>
            </div>
        </div>
    </div>

</x-app-layout>