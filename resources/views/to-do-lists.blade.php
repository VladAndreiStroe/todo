<x-app-layout>
    <x-slot name="header">
        <h2 class="h4 font-weight-bold">
           To do lists
        </h2>
    </x-slot>

    <div class="card my-4">
        <div class="card-body">

            <div class="row justify-content-end">
                <div class="col-2 ml-auto mb-3">
                    <a href="{{route('to-do-lists-form')}}" class="btn btn-primary float-right">
                        Add new list
                    </a>
                </div>
            </div>

            <table class="table">
                <thead>
                    <tr>
                        <th>Title</th>
                        <th>User</th>
                        <th>Nr. tasks</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($todoLists as $todoList)
                        <tr style="margin:auto">
                            <td style="margin:auto">{{$todoList->title}}</td>
                            <td>{{$todoList->user->email}}</td>
                            <td>{{count($todoList->tasks)}}</td>
                            <td>
                            <a href="{{route('view-list-details',$todoList->id)}}" class="btn btn-info btn-sm">View</a>
                            <a href="{{route('to-do-lists-edit-form',['list'=>$todoList->id])}}" class="btn btn-primary btn-sm">
                                Edit
                            </a>
                            <form method="POST" action="{{route('to-do-list-delete')}}" style="display:inline-block">
                                @csrf
                                <input type="hidden" value="{{$todoList->id}}" name="modelId" />
                                <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                            </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</x-app-layout>