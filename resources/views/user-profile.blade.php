<x-app-layout>
    <x-slot name="header">
        <h2 class="h4 font-weight-bold">
           User Profile
        </h2>
    </x-slot>

    <div class="container">
        <div class="row">
            <div class="col-4">
                <x-user-profile-form :user="$user" :isAuth="$isAuth"></x-user-profile-form>
            </div>
            <div class="col-8">
                <x-user-informations :user="$user" :isAuth="$isAuth"></x-user-informations>
            </div>
        </div>
    </div>
    
</x-app-layout>