<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\ToDoListController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/to-do-lists',[ToDoListController::class, 'index']);
Route::get('/to-do-lists/{list}',[ToDoListController::class,'getList']);
Route::post('/to-do-lists',[ToDoListController::class,'storeToDoList']);
Route::delete('/to-do-lists/{list}',[ToDoListController::class, 'deleteList']);

Route::get("/test",function(){
    return response([
        'name' => "Vlad",
        'email' => 'email@game.ro'
    ]);
});
