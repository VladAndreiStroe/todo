<?php

use App\Http\Controllers\CommentController;
use App\Http\Controllers\CommentReactionController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ToDoListController;
use App\Http\Controllers\TaskController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\ReplyCommentController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\BlogController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

require __DIR__.'/auth.php';


Route::group(['middleware'=>['auth']],function(){
    Route::get('/dashboard', [DashboardController::class,'index'])->name('dashboard');
    Route::get('/to-do-list',[ToDoListController::class,'index'])->name('to-do-lists');
    Route::get('/to-do-list/view/{list}',[ToDoListController::class,"viewListDetails"])->name('view-list-details');
    Route::get('/to-do-list/add',[ToDoListController::class,'addNewListForm'])->name('to-do-lists-form');
    Route::get('/to-do-list/edit',[ToDoListController::class,'editList'])->name('to-do-lists-edit-form');
    Route::post('/to-do-list/edit',[ToDoListController::class,'update'])->name('to-do-lists-edit');
    Route::post('/to-do-list',[ToDoListController::class,'store'])->name('to-do-lists-add');
    Route::post('/to-do-list-delete',[ToDoListController::class,'delete'])->name('to-do-list-delete');


    Route::get('/to-do-list/view/{list}/add-task',[TaskController::class,'addNewTaskView'])->name('add-new-task-view');
    Route::get('/task/{task}',[TaskController::class,'updateTaskView'])->name('update-task-view');
    Route::post('/store-task',[TaskController::class,"store"])->name('store-new-task');
    Route::post('/update-task',[TaskController::class,'updateTask'])->name('update-task');
    Route::post('/task-delete',[TaskController::class,'delete'])->name('delete-task');
    Route::get('/view-task/{task}',[TaskController::class,'viewTaskView'])->name('view-task');

    Route::post("/add-comment",[CommentController::class,'store'])->name('add-comment');
    Route::post('/add-reply',[ReplyCommentController::class,'store'])->name('add-reply');


    Route::get('/user-profile',[UserController::class,'userProfileView'])->name('user-profile');
    Route::get('/user-profile/{user}',[UserController::class,'userProfileView'])->name('user-profile-noauth');
    Route::post('/user-profile',[UserController::class,'updateUser'])->name('user-profile-update');
    Route::post('/add-profile-picture',[UserController::class,'addProfilePicture'])->name('add-profile-picture');

    Route::post('/add-reaction',[CommentReactionController::class,'store'])->name('add-reaction');

    Route::get('/blog',[BlogController::class,'index'])->name('blog');
    Route::get('/post-add',[BlogController::class,'addForm'])->name("add-post-form");
    Route::get('/post',[\App\Http\Controllers\BlogController::class,'getPost'])->name('get-post');
    Route::post('/post',[BlogController::class,"store"])->name('add-new-post');

    Route::get("/testconfig",[BlogController::class,'test']);
});






